package com.my.bookstore.util;


public class MessageUtil {
    private MessageUtil() {
        throw new IllegalAccessError(MessageUtil.class.toString());
    }

    public static final String ERROR_BOOK = "No book exist with that id";
}
