package com.my.bookstore.util;

public enum DataBaseEnum {

    DB_HOST, DB_QUERY_TIMEOUT, DB_USER, DB_PASSWORD, DB_POOL_SIZE, DB_DATABASE, DB_SCHEMA
}
