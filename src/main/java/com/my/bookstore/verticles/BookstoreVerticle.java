package com.my.bookstore.verticles;

import com.my.bookstore.dao.JdbcRepositoryWrapper;
import com.my.bookstore.service.business.BooksService;
import com.my.bookstore.service.business.BooksServiceImpl;
import com.my.bookstore.service.crud.BooksServiceCRUD;
import com.my.bookstore.service.crud.BooksServiceCRUDImpl;
import com.my.bookstore.util.AppEnum;
import com.my.bookstore.util.SystemUtil;
import com.my.bookstore.verticles.api.BooksVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class BookstoreVerticle extends RestAPIVerticle {

    public static final String API_BOOKSTORE = "/api/bookstore";

    private static final String API_HEALTHCHECK = API_BOOKSTORE + "/healthcheck";

    private BooksService booksService;
    private BooksServiceCRUD booksServiceCRUD;

    @Override
    public void start() throws Exception {
        super.start();
        //map environment

        //jdbc
        final JdbcRepositoryWrapper jdbc = JdbcRepositoryWrapper.getInstance(vertx);
        this.booksServiceCRUD = new BooksServiceCRUDImpl(jdbc);
        this.booksService = new BooksServiceImpl(this.booksServiceCRUD);

        Router router = Router.router(vertx);
        // Body Handler
        router.route().handler(BodyHandler.create());

        this.enableCorsSupport(router);
        this.loadRoute(router);
        this.deployRestVerticle(router);

        this.createHttpServer(router, SystemUtil.getEnvironmentIntValue(AppEnum.APP_PORT.name()));

    }

    public void loadRoute(Router router) {
        // Healthcheck
        router.get(API_HEALTHCHECK).handler(this::apiHealthCheck);
    }

    private void deployRestVerticle(Router router) {
        vertx.deployVerticle(new BooksVerticle(router, this.booksService));
    }

    private void apiHealthCheck(RoutingContext context) {
        context.response().setStatusCode(200).setStatusMessage("OK").end();
    }
}
