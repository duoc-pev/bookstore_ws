package com.my.bookstore.verticles.api;

import com.my.bookstore.service.business.BooksService;
import com.my.bookstore.verticles.BookstoreVerticle;
import com.my.bookstore.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class BooksVerticle extends RestAPIVerticle {

    private static final String API_BOOKS = BookstoreVerticle.API_BOOKSTORE.concat("/v1/books");
    private static final String API_BOOK_BY_ID = API_BOOKS.concat("/:id");

    private static final String ID = "id";
    private BooksService booksService;

    public BooksVerticle(Router router, BooksService booksService) {
        //routes
        this.loadRoute(router);

        //services
        this.booksService = booksService;
    }

    private void loadRoute(Router router) {
        router.get(API_BOOK_BY_ID).handler(this::apiGetBookById);
    }

    private void apiGetBookById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String bookId = context.request().getParam(ID);
            this.booksService.getBookInventoryById(bookId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

}
