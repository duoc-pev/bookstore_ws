package com.my.bookstore.model.store;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class Inventory {

    private Integer totalAvailable;
    private List<BooksAvailable> booksByBranchOffice;
}