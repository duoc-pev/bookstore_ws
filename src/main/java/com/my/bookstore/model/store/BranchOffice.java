package com.my.bookstore.model.store;

import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
public class BranchOffice {

    private Integer id;
    private String name;

    public BranchOffice(JsonObject json) {
        this.id = json.getInteger("branch_id");
        this.name = json.getString("branch_name");
    }
}
