package com.my.bookstore.model.store;

import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BooksAvailable {

    private BranchOffice branchOffice;
    private Integer quantity;

    public BooksAvailable(JsonObject json) {
        this.branchOffice = new BranchOffice(json);
        this.quantity = json.getInteger("quantity");
    }
}
