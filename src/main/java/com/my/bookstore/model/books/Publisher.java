package com.my.bookstore.model.books;

import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
public class Publisher {

    private Integer id;
    private String name;

    public Publisher(JsonObject json) {
        this.id = json.getInteger("publisher_id");
        this.name = json.getString("publisher_name");
    }
}
