package com.my.bookstore.model.books;

import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
public class Author {

    private Integer id;
    private String firstName;
    private String lastName;
    private String companyName;

    public Author(JsonObject json) {
        this.id = json.getInteger("author_id");
        this.firstName = json.getString("author_fname");
        this.lastName = json.getString("author_lname");
        this.companyName = json.getString("author_company_name");
    }
}
