package com.my.bookstore.model.books;

import com.my.bookstore.model.store.Inventory;
import lombok.Data;

@Data
public class BookResponse {

    private Book book;
    private Inventory inventory;

    public BookResponse() {
        this.book = new Book();
        this.inventory = new Inventory();
    }
}
