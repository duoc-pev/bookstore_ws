package com.my.bookstore.model.books;

import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Book {

    private String isbn;
    private String title;
    private String publicationDate;
    private Integer edition;
    private Integer availableQuantity;
    private String price;
    private Author author;
    private Publisher publisher;

    public Book(JsonObject json) {
        this.isbn = json.getString("book_isbn");
        this.title = json.getString("book_title");
        this.publicationDate = json.getString("book_publication");
        this.edition = json.getInteger("book_edition");
        this.availableQuantity = json.getInteger("book_available_q");
        this.price = json.getString("book_price");
        this.author = new Author(json);
        this.publisher = new Publisher(json);
    }
}
