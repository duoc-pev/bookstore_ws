package com.my.bookstore.service.crud;

import com.my.bookstore.model.books.Book;
import com.my.bookstore.model.store.BooksAvailable;
import io.vertx.core.Future;

import java.util.List;

public interface BooksServiceCRUD {

    Future<Book> getBookById(String bookId);

    Future<List<BooksAvailable>> getBooksInventory(String bookId);
}
