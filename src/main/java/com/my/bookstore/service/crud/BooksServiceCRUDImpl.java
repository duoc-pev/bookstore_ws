package com.my.bookstore.service.crud;

import com.my.bookstore.dao.BooksDAO;
import com.my.bookstore.dao.JdbcRepositoryWrapper;
import com.my.bookstore.model.books.Book;
import com.my.bookstore.model.store.BooksAvailable;
import io.vertx.core.Future;

import java.util.List;

public class BooksServiceCRUDImpl implements BooksServiceCRUD {

    private BooksDAO booksDAO;

    public BooksServiceCRUDImpl(JdbcRepositoryWrapper jdbc) {
        this.booksDAO = BooksDAO.getInstance(jdbc);
    }

    @Override
    public Future<Book> getBookById(String bookId) {
        return this.booksDAO.getBookById(bookId);
    }

    @Override
    public Future<List<BooksAvailable>> getBooksInventory(String bookId) {
        return this.booksDAO.getBooksInventory(bookId);
    }
}
