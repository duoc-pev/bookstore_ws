package com.my.bookstore.service.business;

import com.my.bookstore.model.books.BookResponse;
import com.my.bookstore.model.store.BooksAvailable;
import com.my.bookstore.service.crud.BooksServiceCRUD;
import io.vertx.core.Future;

import java.util.List;

public class BooksServiceImpl implements BooksService {

    private final BooksServiceCRUD booksServiceCRUD;

    public BooksServiceImpl(final BooksServiceCRUD booksServiceCRUD) {
        this.booksServiceCRUD = booksServiceCRUD;
    }

    @Override
    public Future<BookResponse> getBookInventoryById(String bookId) {
        Future<BookResponse> future = Future.future();
        BookResponse response = new BookResponse();

        this.booksServiceCRUD.getBookById(bookId).compose(book -> {
            response.setBook(book);

            Future<List<BooksAvailable>> bookFuture = Future.future();

            this.booksServiceCRUD.getBooksInventory(bookId).setHandler(bookFuture.completer());

            return bookFuture;
        }).compose(inventory -> {
            response.getInventory().setBooksByBranchOffice(inventory);
            response.getInventory().setTotalAvailable(response.getBook().getAvailableQuantity());

            future.complete(response);
        }, future);

        return future;
    }
}
