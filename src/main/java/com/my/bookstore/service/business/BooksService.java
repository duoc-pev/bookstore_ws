package com.my.bookstore.service.business;

import com.my.bookstore.model.books.BookResponse;
import io.vertx.core.Future;

public interface BooksService {

    Future<BookResponse> getBookInventoryById(String bookId);
}
