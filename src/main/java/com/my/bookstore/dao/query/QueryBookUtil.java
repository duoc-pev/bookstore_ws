package com.my.bookstore.dao.query;

import static com.my.bookstore.util.Constant.DB_SCHEMA;
import static com.my.bookstore.util.Constant.FROM;
import static com.my.bookstore.util.Constant.LEFT_OUTER_JOIN;
import static com.my.bookstore.util.Constant.SELECT;
import static com.my.bookstore.util.Constant.WHERE;

public class QueryBookUtil {

    private QueryBookUtil() {
        throw new IllegalAccessError(QueryBookUtil.class.toString());
    }

    public static String selectBranchOfficeInventory() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" br.id as branch_id, ");
        query.append(" br.name as branch_name, ");
        query.append(" bi.quantity ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".branch br ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" ");
        query.append(DB_SCHEMA);
        query.append(".branch_inventory bi on (bi.branch_id = br.id AND bi.book_isbn = ?) ");

        return query.toString();
    }

    public static String getBookByIdSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" bo.isbn = ? ");

        return selectBook().append(where).toString();
    }

    public static String getBookByIdSqlTest() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" bo.isbn = '978-62-82077-46-4' ");

        return selectBook().append(where).toString();
    }

    private static StringBuilder selectBook() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectBookOnly());
        query.append(fromWhereBooks().toString());

        return query;
    }

    private static String selectBookOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" bo.isbn as book_isbn, ");
        query.append(" bo.title as book_title, ");
        query.append(" bo.publication_date as book_publication, ");
        query.append(" bo.edition as book_edition, ");
        query.append(" bo.available_quantity as book_available_q, ");
        query.append(" bo.price as book_price, ");
        query.append(" a.id as author_id, ");
        query.append(" a.first_name as author_fname, ");
        query.append(" a.second_name as author_lname, ");
        query.append(" a.company_name as author_company_name, ");
        query.append(" p.id as publisher_id, ");
        query.append(" p.name as publisher_name ");

        return query.toString();
    }

    private static StringBuilder fromWhereBooks() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".books bo ")
                .append(LEFT_OUTER_JOIN).append(" ")
                .append(DB_SCHEMA)
                .append(".authors a on (a.id = bo.author) ")
                .append(LEFT_OUTER_JOIN).append(" ")
                .append(DB_SCHEMA)
                .append(".publishers p on (p.id = bo.publisher) ");

        return query;
    }

}
