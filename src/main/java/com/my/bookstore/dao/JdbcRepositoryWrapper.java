package com.my.bookstore.dao;

import com.my.bookstore.util.AppEnum;
import com.my.bookstore.util.DataBaseEnum;
import com.my.bookstore.util.SystemUtil;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;

import java.util.List;
import java.util.Optional;

public class JdbcRepositoryWrapper {

    private static JdbcRepositoryWrapper instance;
    private final SQLClient client;

    private JdbcRepositoryWrapper(Vertx vertx) {
        client = PostgreSQLClient.createShared(vertx, this.config());
    }


    public static synchronized JdbcRepositoryWrapper getInstance(Vertx vertx) {
        if (instance == null) {
            instance = new JdbcRepositoryWrapper(vertx);
        }
        return instance;
    }

    /**
     * Suitable for `add`, `exists` operation.
     *
     * @param params        query params
     * @param sql           sql
     * @param resultHandler async result handler
     */
    public void executeNoResult(JsonArray params, String sql, Handler<AsyncResult<Void>> resultHandler) {

        client.getConnection(connHandler(resultHandler, connection -> connection.updateWithParams(sql, params, r -> {
            connection.close();
            if (r.succeeded()) {
                resultHandler.handle(Future.succeededFuture());
            } else {

                resultHandler.handle(Future.failedFuture(r.cause()));

            }
        })));
    }

    public void executeOneResult(JsonArray params, String sql, Handler<AsyncResult<Optional<JsonObject>>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.queryWithParams(sql, params, r -> {
            connection.close();
            if (r.succeeded()) {
                List<JsonObject> resList = r.result().getRows();
                if (resList == null || resList.isEmpty()) {
                    resultHandler.handle(Future.succeededFuture(Optional.empty()));

                } else {
                    resultHandler.handle(Future.succeededFuture(Optional.of(resList.get(0))));
                }

            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    public void retrieveAll(String sql, Handler<AsyncResult<List<JsonObject>>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.query(sql, r -> {
            connection.close();
            if (r.succeeded()) {
                resultHandler.handle(Future.succeededFuture(r.result().getRows()));
            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }


    public void retrieveMany(JsonArray params, String sql, Handler<AsyncResult<List<JsonObject>>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.queryWithParams(sql, params, r -> {
            connection.close();
            if (r.succeeded()) {
                resultHandler.handle(Future.succeededFuture(r.result().getRows()));
            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    /**
     * A helper methods that generates async handler for SQLConnection
     *
     * @return generated handler
     */
    private <R> Handler<AsyncResult<SQLConnection>> connHandler(Handler<AsyncResult<R>> h1, Handler<SQLConnection> h2) {
        return conn -> {
            if (conn.succeeded()) {
                final SQLConnection connection = conn.result();
                h2.handle(connection);
            } else {
                h1.handle(Future.failedFuture(conn.cause()));
            }
        };
    }


    private JsonObject config() {
        final String username = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_USER.name());
        final String password = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_PASSWORD.name());
        final String database = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_DATABASE.name());
        final int maxPoolSize = SystemUtil.getEnvironmentIntValue(DataBaseEnum.DB_POOL_SIZE.name());
        final int queryTimeout = SystemUtil.getEnvironmentIntValue(DataBaseEnum.DB_QUERY_TIMEOUT.name());
        final String databaseHost = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_HOST.name());
        final int appPort = SystemUtil.getEnvironmentIntValue(AppEnum.APP_PORT.name());

        return new JsonObject()
                .put("http.port", appPort)
                .put("username", username)
                .put("password", password)
                .put("maxPoolSize", maxPoolSize)
                .put("database", database)
                .put("host", databaseHost)
                .put("queryTimeout", queryTimeout);

    }

}