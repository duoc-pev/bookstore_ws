package com.my.bookstore.dao;

import com.my.bookstore.dao.query.QueryBookUtil;
import com.my.bookstore.exception.AppException;
import com.my.bookstore.model.books.Book;
import com.my.bookstore.model.store.BooksAvailable;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.my.bookstore.util.MessageUtil.ERROR_BOOK;


public class BooksDAO {

    private static BooksDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private BooksDAO(final JdbcRepositoryWrapper jdbc) {

        this.jdbc = jdbc;
    }

    public static synchronized BooksDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (instance == null) {
            instance = new BooksDAO(jdbc);
        }
        return instance;
    }


    public Future<Book> getBookById(String bookId) {
        Future<Book> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(bookId), QueryBookUtil.getBookByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();
                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Book(jsonObjectOptional.get()));
                } else {
                    future.fail(new AppException(ERROR_BOOK));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });
        return future;
    }

    public Future<List<BooksAvailable>> getBooksInventory(String bookId) {
        Future<List<BooksAvailable>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(bookId), QueryBookUtil.selectBranchOfficeInventory(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(BooksAvailable::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

}